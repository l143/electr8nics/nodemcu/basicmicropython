# Script en NodeMCU

Para interactuar con el REPL de python en el NodeMCU, podemos apoyarnos de cualquier cliente serial.  

Es importante comentar que en linux, el usuario que ejecute el cliente deberá tener permiso al dispositivo */dev/ttyUSB0* o el que corresponda.
 
## REPL
El repl de micropython puede ser ejecutado con ayuda de [screen](https://gitlab.com/l143/linux/multiplexors/screen) o algún similar. Esto nos permitirá relizar validaciones sencillas y elecutar código de prueba en el NodeMCU.

> Creamos una sesión con screen al dispositivo seteando el baudRate 
```bash
$ sudo screen /dev/ttyUSB0 115200
```

## LOAD

Si queremos hacer cosas más interesnates como listar o intercambiar archivos en tre nuestro computador y el NodeMCU podemos usar [ampy](https://pypi.org/project/adafruit-ampy/)

> Instalación 
```bash
$ pip install adafruit-ampy
```
> Listar los archivos en el dispositivo
```
$ ampy --port /dev/ttyUSB0 --baud 115200 ls
```

> Obtener un archivo del NodeMCU  
*Si se omite el fileDestination, solo se mostrará el contenido en la terminal.*
```
$ ampy --port /dev/ttyUSB0 --baud 115200 get [fileName] [fileDestination]
```

> Mandar un archivo al NodeMCU 
``` 
$ ampy --port /dev/ttyUSB0 --baud 115200 put [localFileName]
```