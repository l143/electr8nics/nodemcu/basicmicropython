# WebRepl

Se trata de una versión web del repl que podemos usar para experimentar o hacer cosas al vuelo.

Para poder usarlo hay que cargar el main.py al dispositivo. Esto permitirá activar el webrepl sin conectarnos a su red.

Primero y por medio de una sesión screen, activaremos el webrepl.

```python
import webrepl_setup
```

Esto nos preguntará di deseamos activar el webrepl al bootear *(E)* y nos pedirá setear una contraseña y reiniciar el NodeMCU para poder conectarnos.

Después de reiniciar, debemos iniciar el servidor webrepl en el NodeMCU.
```python
import webrepl
webrepl.start()
```
Esto nos arrojará la ip y el puerto dónde se levanta el servicio.

Si tenemos duda de la ip de nuestra red local, podemos ejecutar lo siguiente para obtener la información corecta.

```python
import network
wlan = network.WLAN(network.STA_IF)
wlan.ipconfig()
```

Ya con la ip, procederemos a bajar el repositorio del [cliente webrepl](https://github.com/micropython/webrepl) y levantaremos un servidor simple en nuestro computador para poder usarlo.


```bash
$ (env) git clone git@github.com:micropython/webrepl.git
$ (env) cd webrepl
$(env) python -m http.server
```

Solo queda poner la ip en el sitio e ingresar al webrepl.