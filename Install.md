# Mandar el firmware al [nodemcu](https://github.com/nodemcu/nodemcu-devkit-v1.0)


> Opcional  
*Descargar versión de [micropython](https://micropython.org/download/esp8266/).*

Instalar esptool para copiar el firmware.

``` bash
$ python3 -m venv env
$ source env/bin/activate
$ pip install esptool
```

Borrar la flash (sudo)

``` bash
$(env) esptool.py --port /dev/ttyUSB0 erase_flash
```
- *El puerto depende del sistema operativo y dispositivos conectados*.

Cargar el firmware. (sudo)

``` bash
esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 ./bin/esp8266-20200911-v1.13.bin
```
