# Simple Led blinking

Led con parpadeo intermitente.

Para ver funcionar este proyecto debe conectar un led externo *(con una resistencia si lo cree conveniente)* en el GPIO 16 *pin D0)*

> Carga del proyecto
```bash
$ ampy --port /dev/ttyUSB0 --baud 115200 put main.py
```

![](./example.mp4)
