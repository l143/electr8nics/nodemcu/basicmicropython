from machine import Pin
import time

log_file = "log.txt"

led = Pin(16, Pin.OUT) # Definimos el pin  16 como salida
led.value(0)


def log(message):
    with open(log_file, "a") as f:
        f.write(message + "\n")


def blink():
    while True:
        if led.value() == 1:
            log("off")
            led.off()
        else:
            led.on()
            log("on")

        time.sleep_ms(500)

log("starting")
blink()
