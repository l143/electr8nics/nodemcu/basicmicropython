# NODEMCU

## Contenido

Contenido creado sobre ejemplos con micropython y el NodeMCU.

### Introducción
- [Instalación del firmware](./Install.md)
- [Carga de código](./Load.md)
### Ejemplos
- [WebRepl](./1_WebRepl)
- [SimpleLed](./2_SimpleLed)
- [BasicHttp](./3_BasicHttp)