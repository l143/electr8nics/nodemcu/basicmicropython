# Basic HTTP

## Introducción.

El módulo **ESP8266** cuenta con dos interfaces de red que mycropython mapea a través del módulo network  como:
 - network.STA_IF (station)
 - network.AP_IF (access point)


Este código demuestra la forma de levantar un servidor y reponder con un status 200.

***Requiere las credenciales de la red a la cual se conectará.***

> Carga del proyecto
```bash
$ ampy --port /dev/ttyUSB0 --baud 115200 put main.py
```

![](demo.mp4)

## Referencias
 - https://docs.micropython.org/en/latest/esp8266/tutorial/network_basics.html