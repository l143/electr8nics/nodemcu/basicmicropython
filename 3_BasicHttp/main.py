from machine import RTC
import network
import socket

rtc = RTC()

PORT = 80
SSID = "SOME"
PASSWORD = "PASS"

class MySocket:
    def __init__(self, address_damily, protocol):
        self.a_f = address_damily
        self.protocol = protocol
        self.socket = None

    def __enter__(self):
        self.socket = socket.socket(self.a_f, self.protocol)
        return self.socket

    def __exit__(self, exc_type, exc_value, traceback ):
        self.socket.close()

def log(message, level="INFO"):
    now = rtc.datetime()
    now_str = "[{}/{}/{}-{}:{}:{}]".format(now[0], now[1], now[2], now[4], now[5], now[6])
    full_msg = "{} {} {}".format(now_str, level, message)
    print(full_msg)

def network_connect():
    sta = network.WLAN(network.STA_IF)
    if not sta.isconnected():
        sta.active(True)
        sta.connect(SSID, PASSWORD)
        log("Connecting to {}".format(SSID))
        while not sta.isconnected():
            pass

    log("Host ip: " + sta.ifconfig()[0])


def start_server():
    with MySocket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('0.0.0.0', PORT))
        s.listen(5)
        log("Server Ok")
        while True:
            conn, addr = s.accept()
            
            log("Client => {}".format(addr))
            conn.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
            conn.close()

log("ⓛⓐⓘⓝ")
try:
    network_connect()
    start_server()
except Exception as e:
    pass